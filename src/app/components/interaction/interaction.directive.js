class InteractionDirective {
  constructor ($templateCache, Calculate, $window, $rootScope) {
    this.Calculate = Calculate;
    this.$rootScope = $rootScope;

    let directive = {
      restrict: 'E',
      scope: {
        interactionLimit: '='
      },
      template: $templateCache.get('interactionDirective'),
      link: linkFunc,
      transclude: true
    };

    let interaction = this.interaction.bind(this);
    let resize = this.resize.bind(this);

    return directive;

    function linkFunc(scope, element) {
      scope.$root.interactionCount = 1;
      scope.doInteraction = () => interaction(scope);

      angular.element($window).on('resize', () => scope.$apply(() => resize(scope, element)));
      resize(scope, element);
    }
  }

  resize(scope, element) {
    scope.area = this.Calculate.area(element);
  }

  interaction(scope) {
    if (this.$rootScope.interactionCount >= scope.interactionLimit) {
      this.$rootScope.interactionCount = 0;
    }

    this.$rootScope.interactionCount++;
  }
}

export default ($templateCache, Calculate, $window, $rootScope) =>
  new InteractionDirective($templateCache, Calculate, $window, $rootScope);
