class CalculateService {
  area($element) {
    let element = $element[0];
    return element.clientWidth * element.clientHeight;
  }
}

export default CalculateService;
