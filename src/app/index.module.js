import CalculateService from './components/calculate/calculate.service';
import InteractionDirective from './components/interaction/interaction.directive';

angular.module('test', [])
  .service('Calculate', CalculateService)
  .directive('interaction', InteractionDirective);
