Angular Exam
============

Installation
------------

- Use Node 0.12
- Use Ruby 2.2.0

To install the dependencies:

- `npm install`
- `bower install`
- `bundle install`

To run the project:

- `gulp serve`

To build the project

- `gulp build`
