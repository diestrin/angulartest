'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'uglify-save-license', 'del']
});


gulp.task('html', [], function () {
  return gulp.src(path.join(conf.paths.src, '/*.html'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  gulp.src(path.join(conf.paths.bower, '/angular/angular.js'))
    .pipe(gulp.dest(path.join(conf.paths.dist, conf.paths.bower, '/angular/')));

  return gulp.src(path.join(conf.paths.tmp, '/serve/**/*'))
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});


gulp.task('clean', function (done) {
  $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')], done);
});

gulp.task('build', ['html', 'styles', 'scripts'], function () {
  gulp.start('other');
});
