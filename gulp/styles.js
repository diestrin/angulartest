'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var execSync = require('child_process').execSync;

var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')();

gulp.task('styles', function () {
  var sassOptions = {
    style: 'expanded',
    compass: true,
    loadPath: (execSync('gem environment gemdir') + '/gems/breakpoint-2.5.0/stylesheets/').replace(/\n/,'')
  };

  var cssFilter = $.filter('**/*.css');

  return gulp.src([
    path.join(conf.paths.src, '/app/index.scss')
  ])
    .pipe($.rubySass(sassOptions)).on('error', conf.errorHandler('RubySass'))
    .pipe(cssFilter)
    .pipe($.sourcemaps.init({ loadMaps: true }))
    .pipe($.sourcemaps.write())
    .pipe(cssFilter.restore())
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app/')))
    .pipe(browserSync.reload({ stream: true }));
});
